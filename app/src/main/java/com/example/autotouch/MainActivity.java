package com.example.autotouch;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ServiceInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    LinearLayout mainLayout;
    File scriptsFile;
    JSONObject scriptsList;

    public void onResume() {
        super.onResume();
        this.mainLayout.removeAllViews();
        listScripts();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_main);
        this.scriptsFile = new File(getFilesDir(), getString(R.string.fileName));
        this.mainLayout = (LinearLayout) findViewById(R.id.scrollLayout);

        Button more = new Button(this);
        more.setText(getString(R.string.add));
        more.setId(View.generateViewId());
        more.setOnClickListener(v -> {
            if (isServiceEnabled()) {
                Intent intent = new Intent(this, MyAccessibilityService.class);
                intent.putExtra("type", "record");
                stopService(intent);
                startService(intent);
                return;
            }
            requestEnableService();
        });

        ConstraintLayout parent = (ConstraintLayout) this.mainLayout.getParent().getParent();
        parent.setId(View.generateViewId());
        parent.addView(more);
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(parent);
        constraintSet.connect(more.getId(), ConstraintSet.START, parent.getId(), ConstraintSet.START, 0);
        constraintSet.connect(more.getId(), ConstraintSet.END, parent.getId(), ConstraintSet.END, 0);
        constraintSet.connect(more.getId(), ConstraintSet.BOTTOM, parent.getId(), ConstraintSet.BOTTOM, 0);
        constraintSet.applyTo(parent);

        listScripts();
    }

    public void listScripts() {
        TextView alertContent = new TextView(this);
        alertContent.setText(R.string.overlayAlert);
        alertContent.setTextColor(Color.RED);
        alertContent.setOnClickListener(v -> {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            startActivity(intent);
        });
        if(alertContent.getParent() != null) mainLayout.removeView(alertContent);
        if (!Settings.canDrawOverlays(this)) mainLayout.addView(alertContent);

        try {
            this.scriptsList = new JSONObject(readFile(this.scriptsFile));
            if (this.scriptsList.length() != 0) {
                JSONArray names = this.scriptsList.names();
                int i = 0;
                while (true) {
                    if (i < (names != null ? names.length() : 0)) {
                        LinearLayout flexBox = new LinearLayout(this);
                        String scriptName = names.getString(i);
                        JSONObject script = this.scriptsList.getJSONObject(scriptName);
                        String gestures = this.scriptsList.getString(scriptName);

                        TextView textView = new TextView(this);
                        textView.setText(scriptName);
                        textView.setLayoutParams(new LinearLayout.LayoutParams(200, LinearLayout.LayoutParams.WRAP_CONTENT));

                        flexBox.setOnClickListener(v -> {
                            Intent intent = new Intent(this, GesturesActivity.class);
                            intent.putExtra("gestures", gestures);
                            intent.putExtra("name", scriptName);
                            startActivity(intent);
                        });
                        flexBox.addView(textView);

                        Button play = new Button(this);
                        play.setText(getString(R.string.play));
                        play.setOnClickListener(v -> play(scriptName));
                        flexBox.addView(play);

                        Button edit = new Button(this);
                        edit.setText(getString(R.string.edit));
                        edit.setOnClickListener(v -> edit(scriptName, script));
                        flexBox.addView(edit);

                        Button delete = new Button(this);
                        delete.setText(getString(R.string.delete));
                        delete.setOnClickListener(v -> {
                            ArrayList<Object> list = deleteModal(this, getString(R.string.deleteWarning));
                            PopupWindow deletePopupWindow = (PopupWindow) list.get(0);
                            Button deleteConfirm = (Button) list.get(1);
                            deletePopupWindow.showAtLocation(this.mainLayout, Gravity.CENTER, 0, 0);
                            deleteConfirm.setOnClickListener(v1 -> {
                                this.scriptsList.remove(scriptName);
                                writeFile(this.scriptsList.toString(), this.scriptsFile);
                                this.mainLayout.removeView(flexBox);
                                Toast.makeText(this, R.string.deletedToast, Toast.LENGTH_SHORT).show();
                                deletePopupWindow.dismiss();
                            });
                        });
                        flexBox.addView(delete);

                        this.mainLayout.addView(flexBox);
                        i++;
                    } else {
                        return;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void play(String scriptName) {
        if (isServiceEnabled()) {
            Intent intent = new Intent(this, MyAccessibilityService.class);
            intent.putExtra("type", "play");
            intent.putExtra("script-name", scriptName);
            stopService(intent);
            startService(intent);
            return;
        }
        requestEnableService();
    }

    public void edit(String scriptName, JSONObject script) {
        String str = "1";
        LinearLayout popupView = new LinearLayout(this);
        popupView.setOrientation(LinearLayout.VERTICAL);
        popupView.setBackgroundColor(Color.WHITE);

        TextView nameLabel = new TextView(this);
        nameLabel.setText(getString(R.string.name));
        popupView.addView(nameLabel);

        EditText name = new EditText(this);
        name.setText(scriptName);
        popupView.addView(name);

        TextView speedLabel = new TextView(this);
        speedLabel.setText(getString(R.string.speed));
        popupView.addView(speedLabel);

        EditText speed = new EditText(this);
        speed.setInputType(InputType.TYPE_CLASS_NUMBER);
        try {
            speed.setText(script.has("speed") ? script.getString("speed") : str);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        popupView.addView(speed);

        TextView loopLabel = new TextView(this);
        loopLabel.setText(getString(R.string.loop));
        popupView.addView(loopLabel);

        EditText loop = new EditText(this);
        loop.setInputType(InputType.TYPE_CLASS_NUMBER);
        try {
            if (script.has("loop")) {
                str = script.getString("loop");
            }
            loop.setText(str);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        popupView.addView(loop);

        Button save = new Button(this);
        save.setText(getString(R.string.save));
        popupView.addView(save);

        PopupWindow editPopup = GesturesActivity.newPopup(popupView, this);
        save.setOnClickListener(v1 -> {
            Toast savedToast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
            savedToast.setGravity(Gravity.CENTER, 0, 0);
            try {
                script.put("speed", speed.getText().toString());
                script.put("loop", loop.getText().toString());
                this.scriptsList.put(scriptName, script);
                String scriptsListString = this.scriptsList.toString();
                if (!name.getText().toString().equals(scriptName)) {
                    scriptsListString = scriptsListString.replace(scriptName, name.getText().toString());
                }
                writeFile(scriptsListString, this.scriptsFile);
                savedToast.setText(getString(R.string.savedToast));
            } catch (JSONException e) {
                e.printStackTrace();
                savedToast.setText(getString(R.string.error));
            }
            this.mainLayout.removeAllViews();
            listScripts();
            savedToast.show();
            editPopup.dismiss();
        });
        editPopup.showAtLocation(this.mainLayout, Gravity.CENTER, 0, 0);
    }

    public static ArrayList<Object> deleteModal(Context context, String content) {
        LinearLayout popupView = new LinearLayout(context);
        popupView.setOrientation(LinearLayout.VERTICAL);
        popupView.setBackgroundColor(Color.WHITE);

        TextView modalContent = new TextView(context);
        modalContent.setText(content);
        modalContent.setGravity(Gravity.CENTER);
        popupView.addView(modalContent);

        LinearLayout actions = new LinearLayout(context);
        actions.setGravity(Gravity.CENTER);

        Button cancel = new Button(context);
        cancel.setText(context.getString(R.string.cancel));
        actions.addView(cancel);

        Button confirm = new Button(context);
        confirm.setText(context.getString(R.string.confirm));
        actions.addView(confirm);

        popupView.addView(actions);

        PopupWindow popupWindow = GesturesActivity.newPopup(popupView, context);
        cancel.setOnClickListener(v -> popupWindow.dismiss());

        ArrayList<Object> list = new ArrayList<>();
        list.add(popupWindow);
        list.add(confirm);

        return list;
    }

    public void requestEnableService() {
        LinearLayout requestModal = new LinearLayout(this);
        requestModal.setOrientation(LinearLayout.VERTICAL);
        requestModal.setBackgroundColor(Color.WHITE);

        TextView requestContent = new TextView(this);
        requestContent.setText(getString(R.string.requestService, getString(R.string.app_name)));
        requestContent.setGravity(Gravity.CENTER);
        requestModal.addView(requestContent);

        LinearLayout actions = new LinearLayout(this);
        actions.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        actions.setGravity(Gravity.CENTER);

        Button cancel = new Button(this);
        cancel.setText(R.string.cancel);
        Button confirm = new Button(this);
        confirm.setText(R.string.confirm);

        actions.addView(cancel);
        actions.addView(confirm);
        requestModal.addView(actions);

        PopupWindow popupWindow = GesturesActivity.newPopup(requestModal,this);
        popupWindow.showAtLocation(mainLayout, Gravity.CENTER, 0, 0);

        cancel.setOnClickListener(v -> popupWindow.dismiss());
        confirm.setOnClickListener(v -> {
            popupWindow.dismiss();
            Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            startActivity(intent);
        });
    }

    public boolean isServiceEnabled() {
        for (AccessibilityServiceInfo enabledService : ((AccessibilityManager) getSystemService(Context.ACCESSIBILITY_SERVICE)).getEnabledAccessibilityServiceList(-1)) {
            ServiceInfo enabledServiceInfo = enabledService.getResolveInfo().serviceInfo;
            if (enabledServiceInfo.packageName.equals(getPackageName()) && enabledServiceInfo.name.equals(MyAccessibilityService.class.getName())) {
                return true;
            }
        }
        return false;
    }

    public static void writeFile(String str, File scriptsFile2) {
        try {
            FileOutputStream fos = new FileOutputStream(scriptsFile2);
            fos.write(str.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String readFile(File scriptsFile2) {
        byte[] bytes = new byte[((int) scriptsFile2.length())];
        String contents = "{}";
        try {
            FileInputStream fis = new FileInputStream(scriptsFile2);
            int b = fis.read(bytes);
            contents = new String(bytes);
            Log.d("z" + b, contents);
            fis.close();
            return contents;
        } catch (IOException e) {
            e.printStackTrace();
            writeFile("{}", scriptsFile2);
            return contents;
        }
    }
}
