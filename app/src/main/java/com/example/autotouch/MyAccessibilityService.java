package com.example.autotouch;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.content.Context;
import android.content.Intent;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.widget.Button;
import android.widget.TextView;
import java.io.File;
import org.json.JSONException;
import org.json.JSONObject;

public class MyAccessibilityService extends AccessibilityService {
    GestureDescription.Builder builder;
    View floatView;
    GestureDescription gesture;
    int gestureId = 1;
    View glassView;
    WindowManager.LayoutParams glassViewParams = new WindowManager.LayoutParams(
        WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT,
        WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
        WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
        PixelFormat.TRANSLUCENT
    );
    WindowManager.LayoutParams params = new WindowManager.LayoutParams(
        WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT,
        WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
            WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
        PixelFormat.TRANSLUCENT
    );

    JSONObject script = new JSONObject();
    String scriptName = "";
    File scriptsFile;
    WindowManager windowManager = null;
    boolean isDispatching = false;
    boolean isRecording = false;
    long startRecordingTime;
    int loopCount = 0;
    int dispatchTrial = 0;
    int MAX_TRIAL_COUNT = 20;

    public void dispatch(final GestureDescription g) {
        this.glassViewParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        this.windowManager.updateViewLayout(this.glassView, this.glassViewParams);// dont need to be inside?
        this.isDispatching = true;
        dispatchGesture(g, new AccessibilityService.GestureResultCallback() {
            public void onCompleted(GestureDescription gestureDescription) {
                dispatchTrial = 0;
                isDispatching = false;
                glassViewParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
                windowManager.updateViewLayout(glassView, glassViewParams);
            }

            public void onCancelled(GestureDescription gestureDescription) {
                if(dispatchTrial++ < MAX_TRIAL_COUNT) {
                    dispatch(g);
                }
            }
        }, (Handler) null);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        addFloatView();
        Button startOrStop = (Button) this.floatView.findViewById(R.id.startOrStop);
        TextView scriptInfo = (TextView) this.floatView.findViewById(R.id.scriptInfo);
        TextView remove = (TextView) this.floatView.findViewById(R.id.remove);

        remove.setOnClickListener(v -> this.windowManager.removeView(this.floatView));

        if (!intent.hasExtra("type")) {
            return START_NOT_STICKY;
        }
        String type = intent.getStringExtra("type");
        if (type.equals("record")) {
            scriptInfo.setText(R.string.record);
            startOrStop.setOnClickListener(v -> startOrStopClicked(startOrStop));
            return START_NOT_STICKY;
        } else if (!type.equals("play")) {
            return START_NOT_STICKY;
        } else {
            String stringExtra = intent.getStringExtra("script-name");
            this.scriptName = stringExtra;
            scriptInfo.setText(stringExtra);
            startOrStop.setOnClickListener(v -> {
                try {
                    playScript();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            });
            return START_NOT_STICKY;
        }
    }

    public void startOrStopClicked(Button startOrStop) {
        if (!this.isRecording) {
            startOrStop.setText(getString(R.string.stop));
            this.windowManager.addView(this.glassView, this.glassViewParams);
            this.windowManager.removeView(this.floatView);// to make floatView on top off glassView
            this.windowManager.addView(this.floatView, this.params);
            this.script = new JSONObject();
            this.gestureId = 1;
            this.isRecording = true;
            this.startRecordingTime = System.currentTimeMillis();
            return;
        }
        startOrStop.setText(getString(R.string.start));
        this.windowManager.removeView(this.glassView);
        try {
            String scriptsListString = MainActivity.readFile(this.scriptsFile);
            JSONObject scriptsJSON = new JSONObject(scriptsListString);
            scriptsJSON.put("ScriptNo." + scriptsListString.length(), this.script);
            MainActivity.writeFile(scriptsJSON.toString(), this.scriptsFile);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.isRecording = false;
    }

    public void onCreate() {
        LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View floatView2 = mInflater.inflate(R.layout.activity_float, (ViewGroup) null);
        View glassView2 = mInflater.inflate(R.layout.activity_glass, (ViewGroup) null);

        this.windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        this.floatView = floatView2;
        this.glassView = glassView2;
        this.scriptsFile = new File(getFilesDir(), getString(R.string.fileName));
    }

    public void addFloatView() {
        this.floatView.setOnTouchListener(new View.OnTouchListener() {
            private float initialTouchX;
            private float initialTouchY;
            private int initialX;
            private int initialY;

            public void moveFloatView(MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    this.initialX = MyAccessibilityService.this.params.x;
                    this.initialY = MyAccessibilityService.this.params.y;
                    this.initialTouchX = event.getRawX();
                    this.initialTouchY = event.getRawY();
                    return;
                }
                if (event.getAction() == MotionEvent.ACTION_MOVE) {
                    params.x = this.initialX + ((int) (event.getRawX() - this.initialTouchX));
                    params.y = this.initialY + ((int) (event.getRawY() - this.initialTouchY));
                    windowManager.updateViewLayout(floatView, params);
                }
            }

            public boolean onTouch(View v, MotionEvent event) {
                v.performClick();
                moveFloatView(event);
                return true;
            }
        });
        this.glassViewParams.gravity = Gravity.CENTER;
        this.params.gravity = Gravity.START;
        if (this.floatView.getParent() != null) {
            this.windowManager.removeView(this.floatView);
        }
        this.windowManager.addView(this.floatView, this.params);

        this.glassView.setOnTouchListener(new View.OnTouchListener() {
            JSONObject gesture;
            long hoverTime;
            Path path;
            long startTime;

            public void passingTouch(MotionEvent event) throws JSONException {
                int action = event.getAction();
                int coordinateX = (int) event.getRawX();
                int coordinateY = (int) event.getRawY();
                if (action == MotionEvent.ACTION_DOWN) {
                    long hoverDuration;
                    if (gestureId == 1) { // record the first un touch gesture
                        hoverDuration = System.currentTimeMillis() - startRecordingTime;
                    } else {
                        hoverDuration = System.currentTimeMillis() - this.hoverTime;
                    }
                    script.put(String.valueOf(gestureId++), new JSONObject("{duration: " + hoverDuration + "}"));
                    this.gesture = new JSONObject();
                    this.path = new Path();
                    this.path.moveTo(event.getRawX(), event.getRawY());
                    this.startTime = System.currentTimeMillis();
                    this.gesture.put(
                        "points",
                        coordinateX + ";" + coordinateY
                    );
                } else if (action == MotionEvent.ACTION_MOVE) {
                    if (event.getRawX() > 0.0f && event.getRawY() > 0.0f) {
                        this.path.lineTo(event.getRawX(), event.getRawY());
                        this.gesture.put(
                            "points",
                            this.gesture.getString("points") + "-" + coordinateX + ";" + coordinateY
                        );
                    }
                } else if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL) {
                    GestureDescription.Builder builder = new GestureDescription.Builder();
                    this.path.lineTo(event.getRawX(), event.getRawY());
                    long duration = System.currentTimeMillis() - this.startTime;
                    duration = (duration > 0) ? duration : 1;

                    builder.addStroke(new GestureDescription.StrokeDescription(this.path, 0, duration, false));
                    this.gesture.put(
                        "points",
                        this.gesture.getString("points") + "-" + coordinateX + ";" + coordinateY
                    );
                    this.gesture.put("duration", duration);
                    if (!isDispatching) {
                        script.put(String.valueOf(gestureId++), this.gesture); // still record failed gestures
                    }
                    this.hoverTime = System.currentTimeMillis();

                    dispatch(builder.build());
                }
            }

            public boolean onTouch(View v, MotionEvent event) {
                v.performClick();
                try {
                    passingTouch(event);
                    return true;
                } catch (JSONException e) {
                    e.printStackTrace();
                    return true;
                }
            }
        });
    }

    public void playScript() throws JSONException {
        JSONObject scriptsList = new JSONObject(MainActivity.readFile(this.scriptsFile));
        if (scriptsList.has(this.scriptName)) {
            this.loopCount = 0;
            dispatchStroke((JSONObject) scriptsList.get(this.scriptName), 1);
            return;
        }
        Log.d("z", "no script found");
    }

    public GestureDescription.StrokeDescription jsonToStroke(String jsonString, int speed) throws JSONException {
        Path p = new Path();
        p.lineTo(0.0f, 0.0f);
        JSONObject json = new JSONObject(jsonString);
        String pointsString = "";
        if (json.has("points")) {
            pointsString = json.getString("points");
        }
        String pointsString2 = pointsString;
        long duration = Long.parseLong(json.getString("duration")) / ((long) speed);
        String[] points = pointsString2.length() > 0 ? pointsString2.split("-") : new String[0];
        for (int i = 0; i < points.length; i++) {
            String[] coordinate = points[i].split(";");
            if (i == 0) {
                p.moveTo(Float.parseFloat(coordinate[0]), Float.parseFloat(coordinate[1]));
            } else {
                p.lineTo(Float.parseFloat(coordinate[0]), Float.parseFloat(coordinate[1]));
            }
        }
        return new GestureDescription.StrokeDescription(p, 0, (duration <= 0) ? 1 : duration, false);
    }

    public void dispatchStroke(final JSONObject json, final int i) {
        try {
            int loop = json.has("loop") ? json.getInt("loop") : 1;
            if (json.length() != 0) {
                if (json.has(String.valueOf(i))) {
                    String string = json.getString(String.valueOf(i));
                    this.builder = new GestureDescription.Builder();
                    builder.addStroke(jsonToStroke(string, (json.has("speed")) ? json.getInt("speed") : 1));

                    this.gesture = this.builder.build();
                    dispatchGesture(this.gesture, new AccessibilityService.GestureResultCallback() {
                        public void onCompleted(GestureDescription gestureDescription) {
                            int loop = 1;
                            dispatchTrial = 0;
                            try {
                                loop = json.has("loop") ? json.getInt("loop") : 1;
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (i < json.length()) {
                                dispatchStroke(json, i + 1);
                                return;
                            }
                            if (i < loop) {
                                dispatchStroke(json, 1);
                                return;
                            }
                        }

                        public void onCancelled(GestureDescription gestureDescription) {
                            if(dispatchTrial++ < MAX_TRIAL_COUNT) {
                                dispatchStroke(json, i);
                            }
                        }
                    }, (Handler) null);
                    return;
                }
            }
            if (++this.loopCount < loop) {
                dispatchStroke(json, 1);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
    }

    public void onServiceConnected() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void onInterrupt() {
        Log.d("z", "interrupt");
    }
}
