package com.example.autotouch;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import java.io.File;
import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;

public class PointsActivity extends AppCompatActivity {
    boolean isFocusable = false;
    String lastPointTouched = "";
    LinearLayout mainLayout;
    ArrayList<EditText> pointsList = new ArrayList<>();
    RelativeLayout showPoints;
    TextView touch;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_points);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.layout);
        this.mainLayout = linearLayout;

        ArrayList<Object> list = GesturesActivity.createButtons(this, linearLayout);
        Button addConfirm = (Button) list.get(0);
        EditText positionInput = (EditText) list.get(1);
        PopupWindow popupWindow = (PopupWindow) list.get(2);
        Button visualizeButton = (Button) list.get(3);

        findViewById(R.id.back).setOnClickListener(v -> {
            if (this.isFocusable) {
                ArrayList<Object> deleteModalList = MainActivity.deleteModal(this, getString(R.string.unsavedWarning));
                PopupWindow deletePopupWindow = (PopupWindow) deleteModalList.get(0);
                Button deleteConfirm = (Button) deleteModalList.get(1);

                deletePopupWindow.showAtLocation(this.mainLayout, Gravity.CENTER, 0, 0);
                deleteConfirm.setOnClickListener(v1 -> {
                    deletePopupWindow.dismiss();
                    finish();
                });
                return;
            }
            finish();
        });

        addConfirm.setOnClickListener(v -> {
            String input = positionInput.getText().toString();
            if(input.trim().length() > 0) newPoint(" ; ", Integer.parseInt(input));
            popupWindow.dismiss();
        });

        findViewById(R.id.edit).setOnClickListener(v -> {
            this.isFocusable = !this.isFocusable;
            findViewById(R.id.add).setEnabled(this.isFocusable);
            GesturesActivity.edit(this.mainLayout, this.isFocusable);

            Button editButton = (Button) findViewById(R.id.edit);
            if (this.isFocusable) {
                editButton.setText(getString(R.string.save));
                return;
            }
            editButton.setText(getString(R.string.edit));

            Toast savedToast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
            savedToast.setGravity(Gravity.CENTER, 0, 0);
            try {
                save();
                savedToast.setText(getString(R.string.savedToast));
            } catch (JSONException e) {
                e.printStackTrace();
                savedToast.setText(getString(R.string.error));
            }
            savedToast.show();
        });

        ArrayList<Object> VisualizeComponentsList = GesturesActivity.setVisualizeButton(visualizeButton, this, getWindowManager());
        this.showPoints = (RelativeLayout) VisualizeComponentsList.get(0);
        Button close = (Button) VisualizeComponentsList.get(1);

        TextView textView = new TextView(this);
        this.touch = textView;
        textView.setBackgroundColor(Color.YELLOW);

        this.showPoints.setOnTouchListener((v, m) -> {
            this.lastPointTouched = GesturesActivity.touchAtPoint(m, this.touch, this.showPoints);
            return true;
        });

        close.setOnClickListener(v -> {
            visualizeButton.setText(this.lastPointTouched.length() > 0 ? getString(R.string.touchedPoint) + this.lastPointTouched : getString(R.string.visualize));
            getWindowManager().removeView(this.showPoints);
        });

        try {
            JSONObject json = new JSONObject(getIntent().getStringExtra("gesture"));
            String pointsString = json.getString("points");
            String[] points = pointsString.length() > 0 ? pointsString.split("-") : new String[0];
            positionInput.setText(String.valueOf(points.length + 1));

            LinearLayout flexBox = new LinearLayout(this);
            flexBox.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

            TextView durationLabel = new TextView(this);
            durationLabel.setText(getString(R.string.duration));
            flexBox.addView(durationLabel);

            EditText duration = GesturesActivity.newInput(json.getString("duration"), this.isFocusable, this);
            duration.setId(R.id.duration);
            flexBox.addView(duration);

            this.mainLayout.addView(flexBox);

            for (String point : points) {
                newPoint(point, 0);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void newPoint(String str, int i) {
        LinearLayout flexBox = new LinearLayout(this);
        flexBox.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

        String[] coordinate = str.split(";");
        GesturesActivity.addPointToShow(str, this.showPoints, this);

        TextView textViewX = new TextView(this);
        textViewX.setText("X:");
        flexBox.addView(textViewX);

        EditText editTextX = GesturesActivity.newInput(coordinate[0], this.isFocusable, this);
        flexBox.addView(editTextX);

        TextView textViewY = new TextView(this);
        textViewY.setText("Y:");
        flexBox.addView(textViewY);

        EditText editTextY = GesturesActivity.newInput(coordinate[1], this.isFocusable, this);
        flexBox.addView(editTextY);

        Button deleteButton = new Button(this);
        deleteButton.setText(getString(R.string.delete));
        deleteButton.setEnabled(this.isFocusable);
        flexBox.addView(deleteButton);
        deleteButton.setOnClickListener(v -> {
            this.mainLayout.removeView(flexBox);
            this.pointsList.remove(editTextX);
            this.pointsList.remove(editTextY);
        });

        if (0 < i && i <= this.mainLayout.getChildCount()) {
            this.pointsList.add((i - 1) * 2, editTextX);
            this.pointsList.add((i * 2) - 1, editTextY);
            this.mainLayout.addView(flexBox, i);
            return;
        }
        this.pointsList.add(editTextX);
        this.pointsList.add(editTextY);
        this.mainLayout.addView(flexBox);
    }

    public void save() throws JSONException {
        StringBuilder pointsString = new StringBuilder();
        if (this.pointsList.size() > 0) {
            pointsString.append(this.pointsList.get(0).getText());
            for (int i = 1; i < this.pointsList.size(); i++) {
                if (i % 2 == 0) {
                    pointsString.append("-").append(this.pointsList.get(i).getText().toString().trim());
                } else {
                    pointsString.append(";").append(this.pointsList.get(i).getText().toString().trim());
                }
            }
        }
        File scriptFile = new File(getFilesDir(), getString(R.string.fileName));
        String scriptsString = MainActivity.readFile(scriptFile);

        JSONObject gesture = new JSONObject(getIntent().getStringExtra("gesture"));
        gesture.put("duration", ((EditText) findViewById(R.id.duration)).getText().toString());
        gesture.put("points", pointsString.toString());

        MainActivity.writeFile(scriptsString.replace(getIntent().getStringExtra("gesture"), gesture.toString()), scriptFile);
    }
}