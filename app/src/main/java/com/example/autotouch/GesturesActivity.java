package com.example.autotouch;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import java.io.File;
import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;

public class GesturesActivity extends AppCompatActivity {
    ArrayList<ArrayList<EditText>> gesturesList = new ArrayList<>();
    boolean isFocusable = false;
    String lastPointTouched = "";
    LinearLayout mainLayout;
    File scriptsFile;
    RelativeLayout showPoints;
    TextView touch;
    public void onResume() {
        super.onResume();
        refresh();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestures);
        this.mainLayout = (LinearLayout) findViewById(R.id.layout);
        this.scriptsFile = new File(getFilesDir(), getString(R.string.fileName));

        ArrayList<Object> list = createButtons(this, this.mainLayout);
        Button addConfirm = (Button) list.get(0);
        EditText positionInput = (EditText) list.get(1);
        PopupWindow popupWindow = (PopupWindow) list.get(2);
        Button visualizeButton = (Button) list.get(3);

        findViewById(R.id.back).setOnClickListener(v -> {
            if (this.isFocusable) {
                ArrayList<Object> deleteModalList = MainActivity.deleteModal(this, getString(R.string.unsavedWarning));
                PopupWindow deletePopupWindow = (PopupWindow) deleteModalList.get(0);
                Button confirmButton = (Button) deleteModalList.get(1);

                deletePopupWindow.showAtLocation(this.mainLayout, Gravity.CENTER, 0, 0);
                confirmButton.setOnClickListener(v1 -> {
                    deletePopupWindow.dismiss();
                    finish();
                });
                return;
            }
            finish();
        });

        addConfirm.setOnClickListener(v -> {
            try {
                String input = positionInput.getText().toString();
                if(input.trim().length() > 0) newGesture("{'duration': '0', 'points': ' ; - ; '}", Integer.parseInt(input));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            popupWindow.dismiss();

        });

        findViewById(R.id.edit).setOnClickListener(v -> {
            this.isFocusable = !this.isFocusable;
            findViewById(R.id.add).setEnabled(this.isFocusable);
            edit(this.mainLayout, this.isFocusable);

            Button editButton = (Button) findViewById(R.id.edit);
            if (this.isFocusable) {
                editButton.setText(getString(R.string.save));
                return;
            }
            editButton.setText(getString(R.string.edit));

            Toast savedToast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
            savedToast.setGravity(Gravity.CENTER, 0, 0);
            try {
                save();
                savedToast.setText(getString(R.string.savedToast));
            } catch (JSONException e) {
                e.printStackTrace();
                savedToast.setText(getString(R.string.error));
            }
            savedToast.show();
        });

        ArrayList<Object> VisualizeComponentsList = setVisualizeButton(visualizeButton, this, getWindowManager());
        this.showPoints = (RelativeLayout) VisualizeComponentsList.get(0);
        Button close = (Button) VisualizeComponentsList.get(1);

        TextView textView = new TextView(this);
        this.touch = textView;
        textView.setBackgroundColor(Color.YELLOW);

        this.showPoints.setOnTouchListener((v, m) -> {
            lastPointTouched = touchAtPoint(m, touch, showPoints);
            return true;
        });

        close.setOnClickListener(v -> {
            visualizeButton.setText(this.lastPointTouched.length() > 0 ? getString(R.string.touchedPoint) + this.lastPointTouched : getString(R.string.visualize));
            getWindowManager().removeView(this.showPoints);
        });

        try {
            Intent intent = getIntent();
            JSONObject gestures = new JSONObject(intent.getStringExtra("gestures"));
            positionInput.setText(String.valueOf(gestures.length() + 1));

            for (int i = 1; i <= gestures.length(); i++) {
                newGesture(gestures.getString(String.valueOf(i)), 0);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Object> setVisualizeButton(Button visualize, Context context, WindowManager wm) {
        RelativeLayout showPoints = new RelativeLayout(context);
        showPoints.setBackgroundColor(-1);

        Button close = new Button(context);
        close.setText("X");
        showPoints.addView(close);

        visualize.setOnClickListener(v -> wm.addView(showPoints, new WindowManager.LayoutParams()));

        ArrayList<Object> list = new ArrayList<>();
        list.add(showPoints);
        list.add(close);

        return list;
    }

    public static String touchAtPoint(MotionEvent event, TextView touch, RelativeLayout showPoints) {
        RelativeLayout.LayoutParams textViewParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT
        );
        int x = (int) event.getX();
        int y = (int) event.getY();
        textViewParams.setMargins(x, y, 0, 0);
        String lastPointTouched = x + "; " + y;
        touch.setText(lastPointTouched);
        if (touch.getParent() == null) {
            showPoints.addView(touch, textViewParams);
        } else {
            showPoints.updateViewLayout(touch, textViewParams);
        }
        return lastPointTouched;
    }

    public static void addPointToShow(String point, RelativeLayout showPoints2, Context context) { // make input's type = int
        String[] coordinate = point.split(";");
        if (coordinate[0].trim().length() != 0 && coordinate[1].trim().length() != 0) {
            String x = coordinate[0].contains(".") ? coordinate[0].substring(0, coordinate[0].indexOf(".")) : coordinate[0];
            String y = coordinate[1].contains(".") ? coordinate[1].substring(0, coordinate[1].indexOf(".")) : coordinate[1];

            TextView tv = new TextView(context);
            tv.setText("O");

            RelativeLayout.LayoutParams textViewParams = new RelativeLayout.LayoutParams(-2, -2);
            textViewParams.setMargins(Integer.parseInt(x), Integer.parseInt(y), 0, 0);
            showPoints2.addView(tv, textViewParams);
        }
    }

    public static void edit(LinearLayout recurseParent, boolean isFocusable) {
        for (int i = 0; i < recurseParent.getChildCount(); i++) {
            View childAt = recurseParent.getChildAt(i);
            if (childAt instanceof LinearLayout) {
                edit((LinearLayout) recurseParent.getChildAt(i), isFocusable);
            } else if (childAt instanceof Button) {
                if (((Button) childAt).getId() != R.id.back) {
                    ((Button) childAt).setEnabled(isFocusable);
                }
            } else if (childAt instanceof EditText) {
                ((EditText) childAt).setFocusable(isFocusable);
                ((EditText) childAt).setFocusableInTouchMode(isFocusable);
            }
        }
    }

    public void save() throws JSONException {
        JSONObject gestures = new JSONObject();
        for (int i = 0; i < this.gesturesList.size(); i++) {
            JSONObject gesture = new JSONObject();
            ArrayList<EditText> inputs = this.gesturesList.get(i);
            boolean isValid = true;
            String pointsString = "";
            for (int j = 0; j < inputs.size(); j++) {
                String input = inputs.get(j).getText().toString();
                String coordinate = input.trim();
                if (j == 1) {
                    gesture.put("duration", input);
                } else if (j != 2) {
                    if (j != 3) {
                        if (j != 4) {
                            if (j == 5) {
                                if (coordinate.length() == 0) {
                                    isValid = false;
                                } else {
                                    pointsString = pointsString + ";" + coordinate;
                                }
                            }
                        } else if (coordinate.length() == 0) {
                            isValid = false;
                        } else {
                            pointsString = pointsString + "-" + coordinate;
                        }
                    } else if (coordinate.length() == 0) {
                        isValid = false;
                    } else {
                        String pointsString2 = pointsString + ";" + coordinate;
                        String oldPoints = inputs.get(0).getText().toString();
                        pointsString = pointsString2 + (oldPoints.contains(";") ? oldPoints.substring(oldPoints.indexOf("-"), oldPoints.lastIndexOf("-")) : "");
                    }
                } else if (coordinate.length() == 0) {
                    isValid = false;
                } else {
                    pointsString = pointsString + coordinate;
                }
            }
            if (isValid && pointsString.length() > 0) {
                gesture.put("points", pointsString);
            }
            gestures.put(String.valueOf(i + 1), gesture);
        }
        JSONObject scripts = new JSONObject(MainActivity.readFile(this.scriptsFile));
        scripts.put(getIntent().getStringExtra("name"), gestures);
        Log.d("z", scripts.toString());
        MainActivity.writeFile(scripts.toString(), this.scriptsFile);
        refresh();
    }

    public void refresh() {
        this.mainLayout.removeAllViews();
        try {
            JSONObject scripts = new JSONObject(MainActivity.readFile(this.scriptsFile));
            JSONObject gestures = scripts.getJSONObject(getIntent().getStringExtra("name"));

            for (int i = 1; i <= gestures.length(); i++) {
                newGesture(gestures.getString(String.valueOf(i)), 0);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static PopupWindow newPopup(View view, Context context) {
        LinearLayout transBackground = new LinearLayout(context);
        transBackground.setBackgroundColor(Color.parseColor("#80000000"));
        transBackground.setGravity(Gravity.CENTER);
        transBackground.setOrientation(LinearLayout.VERTICAL);
        transBackground.addView(view);

        PopupWindow popupWindow = new PopupWindow(
                transBackground,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                true
        );
        transBackground.setOnClickListener(v -> popupWindow.dismiss());

        return popupWindow;
    }

    public static EditText newInput(String str, boolean isFocusable, Context context) {
        EditText input = new EditText(context);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setId(View.generateViewId());
        input.setText(str);
        input.setFocusable(isFocusable);
        input.setLayoutParams(new LinearLayout.LayoutParams(200, LinearLayout.LayoutParams.WRAP_CONTENT));

        return input;
    }

    public void newGesture(String str, int position) throws JSONException {
        ArrayList<EditText> inputs = new ArrayList<>();
        JSONObject gesture = new JSONObject(str);

        EditText oldPoints = new EditText(this);
        if (gesture.has("points")) {
            String points = gesture.getString("points");
            oldPoints.setText(points);
            for (String point : points.split("-")) {
                addPointToShow(point, this.showPoints, this);
            }
        }
        inputs.add(oldPoints);

        LinearLayout flexBox = new LinearLayout(this);
        flexBox.setOrientation(LinearLayout.VERTICAL);
        LinearLayout flexStartBox = new LinearLayout(this);
        LinearLayout flexEndBox = new LinearLayout(this);

        TextView gestureLabel = new TextView(this);
        gestureLabel.setText("------------");
        flexBox.addView(gestureLabel);

        TextView durationLabel = new TextView(this);
        durationLabel.setText(getString(R.string.duration));

        EditText duration = newInput(gesture.getString("duration"), this.isFocusable, this);
        inputs.add(duration);

        LinearLayout durationFlexItem = new LinearLayout(this);
        durationFlexItem.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        durationFlexItem.addView(durationLabel);
        durationFlexItem.addView(duration);
        flexEndBox.addView(durationFlexItem);

        if (gesture.has("points") && gesture.getString("points").length() > 0) {
            String pointsString = gesture.getString("points");

            LinearLayout firstFlexBox = new LinearLayout(this);
            firstFlexBox.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

            LinearLayout lastFlexBox = new LinearLayout(this);
            lastFlexBox.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

            String[] firstCoordinate = pointsString.substring(0, pointsString.indexOf("-")).split(";");
            TextView firstPointLabel = new TextView(this);
            firstPointLabel.setText(getString(R.string.firstPoint));
            firstFlexBox.addView(firstPointLabel);

            EditText firstPointX = newInput(firstCoordinate[0], this.isFocusable, this);
            inputs.add(firstPointX);
            firstFlexBox.addView(firstPointX);

            TextView separatorX = new TextView(this);
            separatorX.setText(";");
            firstFlexBox.addView(separatorX);

            EditText firstPointY = newInput(firstCoordinate[1], this.isFocusable, this);
            inputs.add(firstPointY);
            firstFlexBox.addView(firstPointY);

            String[] lastCoordinate = pointsString.substring(pointsString.lastIndexOf("-") + 1).split(";");
            TextView lastPointLabel = new TextView(this);
            lastPointLabel.setText(getString(R.string.lastPoint));
            lastFlexBox.addView(lastPointLabel);

            EditText lastPointX = newInput(lastCoordinate[0], this.isFocusable, this);
            inputs.add(lastPointX);
            lastFlexBox.addView(lastPointX);

            TextView separatorY = new TextView(this);
            separatorY.setText(";");
            lastFlexBox.addView(separatorY);

            EditText lastPointY = newInput(lastCoordinate[1], this.isFocusable, this);
            inputs.add(lastPointY);
            lastFlexBox.addView(lastPointY);

            flexStartBox.addView(firstFlexBox);
            flexStartBox.addView(lastFlexBox);

            Button showMoreButtons = new Button(this);
            showMoreButtons.setText(getString(R.string.otherPoints));
            showMoreButtons.setOnClickListener(v -> {
                Intent intent = new Intent(this, PointsActivity.class);
                intent.putExtra("scriptName", getIntent().getStringExtra("name"));
                intent.putExtra("gesture", str);
                startActivity(intent);
            });
            flexEndBox.addView(showMoreButtons);
        }

        Button deleteButtons = new Button(this);
        deleteButtons.setText(getString(R.string.delete));
        deleteButtons.setEnabled(this.isFocusable);
        deleteButtons.setOnClickListener(v -> {
            this.mainLayout.removeView(flexBox);
            this.gesturesList.remove(inputs);
        });
        flexEndBox.addView(deleteButtons);

        flexBox.addView(flexStartBox);
        flexBox.addView(flexEndBox);

        if (0 < position && position <= this.mainLayout.getChildCount()) {
            this.mainLayout.addView(flexBox, position - 1);
            this.gesturesList.add(position - 1, inputs);
            return;
        }
        this.mainLayout.addView(flexBox);
        this.gesturesList.add(inputs);
    }

    public static ArrayList<Object> createButtons(Context context, LinearLayout linearLayout) {
        Button back = new Button(context);
        back.setText("<-");
        back.setId(R.id.back);

        LinearLayout popUp = new LinearLayout(context);
        popUp.setBackgroundColor(Color.WHITE);
        popUp.setGravity(Gravity.CENTER);
        popUp.setOrientation(LinearLayout.VERTICAL);

        TextView label = new TextView(context);
        label.setText(context.getString(R.string.position));
        label.setGravity(Gravity.CENTER);
        popUp.addView(label);

        EditText positionInput = newInput(String.valueOf(1), true, context);
        positionInput.setGravity(Gravity.CENTER);
        popUp.addView(positionInput);

        Button addConfirm = new Button(context);
        addConfirm.setText(context.getString(R.string.add));
        addConfirm.setId(R.id.add);
        popUp.addView(addConfirm);
        PopupWindow popupWindow = newPopup(popUp, context);
        addConfirm.setOnClickListener(v -> popupWindow.dismiss());

        Button more = new Button(context);
        more.setText(context.getString(R.string.add));
        more.setId(R.id.add);
        more.setEnabled(false);
        more.setOnClickListener(v -> popupWindow.showAtLocation(linearLayout, Gravity.CENTER, 0 ,0));

        Button show = new Button(context);
        show.setText(context.getString(R.string.visualize));
        show.setId(View.generateViewId());

        Button edit = new Button(context);
        edit.setText(context.getString(R.string.edit));
        edit.setId(R.id.edit);

        ConstraintLayout parent = (ConstraintLayout) linearLayout.getParent().getParent();
        parent.setId(View.generateViewId());
        parent.addView(back);
        parent.addView(more);
        parent.addView(edit);
        parent.addView(show);
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(parent);

        constraintSet.connect(back.getId(), ConstraintSet.START, parent.getId(), ConstraintSet.START, 0);
        constraintSet.connect(back.getId(), ConstraintSet.TOP, parent.getId(), ConstraintSet.TOP, 0);

        constraintSet.connect(more.getId(), ConstraintSet.START, parent.getId(), ConstraintSet.START, 0);
        constraintSet.connect(more.getId(), ConstraintSet.END, parent.getId(), ConstraintSet.END, 0);
        constraintSet.connect(more.getId(), ConstraintSet.BOTTOM, parent.getId(), ConstraintSet.BOTTOM, 0);

        constraintSet.connect(edit.getId(), ConstraintSet.END, parent.getId(), ConstraintSet.END, 0);

        constraintSet.connect(show.getId(), ConstraintSet.END, edit.getId(), ConstraintSet.START, 0);
        constraintSet.applyTo(parent);

        ArrayList<Object> list = new ArrayList<>();
        list.add(addConfirm);
        list.add(positionInput);
        list.add(popupWindow);
        list.add(show);

        return list;
    }
}